var Srv = new ActiveXObject("SGFED.SGBitmapFontServer");
var Symbol = Srv.GetSymbolObject(0);

var W = Srv.SymbolWidth;
var H = Srv.SymbolHeight;

var name_temp = Srv.FontFileName.split('\\');
var fontname = name_temp[name_temp.length-1].slice(0,-6);

if ((W*H) % 8 == 0)
{
	var fso = new ActiveXObject("Scripting.FileSystemObject");
	var file = fso.CreateTextFile(Srv.FontFileName + ".h", true);

	// Write prologue
	file.WriteLine('// SymbolsCount = ' + Srv.SymbolsCount + ', Width = ' +
	Srv.SymbolWidth + ', Height = ' + Srv.SymbolHeight + '\n');
	
	file.WriteLine('const uint8_t '+ fontname + '_data ['+ Srv.SymbolsCount +']['+ (W*H)/8 +'] = \n{');

	// Write body
	for(var Index = 0; Index < Srv.SymbolsCount; Index++)
	{
		var Line = '\t{';
		Symbol.LoadSymbol(Index);
		var temp_byte = 0;
		temp_byte |= 0;
		for(var BitIndex = 0; BitIndex < W*H; BitIndex++) // Перебираем все пиксели символа
		{
			var Y = BitIndex / W | 0;
			var X = BitIndex % W;
			
			temp_byte |= (Symbol.DataPixels(X, Y) << (7 - (BitIndex % 8)));
			
			if ((BitIndex + 1) % 8 == 0) // Если набралось 8 бит
			{
				if(BitIndex > 8) Line += ', ';
				Line += '0x';
				var len = temp_byte.toString(16).length;
				while(len < 2) // Добавляем нули в начале
				{	
					len++;
					Line += '0';
				}
				Line += temp_byte.toString(16).toUpperCase();
				temp_byte = 0;
			}
			
		}
		Line += '}';
		if(Index < Srv.SymbolsCount - 1) Line += ',';
		Line += '\t // '+ Index + '\t0x' + Index.toString(16).toUpperCase(); // Дописываем в конце код символа
		
		file.WriteLine(Line);
	}

	// Write epilogue
	file.WriteLine('};');
	file.WriteLine('');
	
	file.WriteLine('const tFont '+fontname+' = {(uint8_t *)'+fontname+'_data, '+W+', '+H+', 0, 0};');
	file.WriteLine('//extern const tFont '+fontname+';');

	file.Close();

	Srv.ShowMessage('Export done.');
}
else
{
	Srv.ShowMessage('Wrong symbol size :(');
}
