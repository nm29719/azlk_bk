#include "speed_fuel.h"
#include "main.h"

void update_ADC_val(void);
void update_fuel_flow(void);

vals_data_t val_data;

uint8_t tim_cnt_high = 0; // TODO: ��� � ��������� � static!

int32_t speed_period_sum = 0;
uint32_t speed_count_sum = 0;
int32_t fuel_period = 0;
int32_t speed_tim_last = 0;
int32_t fuel_tim_last = 0;

uint32_t cnt_dist = 0;
uint32_t cnt_fuel = 0;

uint32_t config_dist = 0;

uint32_t Fuel_update_time = 0;
uint32_t cnt_time_update_time = 0;
uint32_t speed_pulse_last = 0;
uint32_t fuel_pulse_last = 0;

uint16_t fuel_buffer[FUEL_BUFFER_LEN][2];
uint8_t fuel_buffer_ptr = 0;
uint8_t fuel_buffer_cnt = 0;

uint16_t flow_dist = 0;
uint16_t flow_fuel = 0;

void Speed_Fuel_proc(void)
{
    // ������� ��������
    //if ((speed_count_sum != 0) && (speed_period_sum != 0)) // ��������� �������� �� ��� � ��/�
    //{
        float speed_temp = 3600000.f / (float)speed_period_sum;//((float)speed_period_sum / (float)speed_count_sum);
        val_data.speed = LOWPASS_F(val_data.speed, speed_temp / config.speed_pulses, 0.1f);
        val_data.speed_cali = LOWPASS_F(val_data.speed_cali, speed_temp / val_data.speed_cali_coef, 0.1f);
    //    speed_count_sum = 0;
    //}
    //else
    	if (RTC_is_Time_passed(speed_pulse_last,2))
    {
    	val_data.speed = 0.f; // ��������� �� ���� 2��� - ���������� ��������
    	val_data.speed_cali = 0.f;
    }

    // ������� ������������� �������
    if (RTC_is_Time_passed(fuel_pulse_last, 2))
        fuel_period = 0;
    if (fuel_period != 0)
    {
    	float fuel_temp = ((1000000.f / ((float) fuel_period)) / PULSES_PER_LITER) * 3600.f; // ��������� ������������ ������ �� ��� � �/�
    	val_data.fuel_moment = LOWPASS_F(val_data.fuel_moment, fuel_temp, 0.3f);
    }
    else
        val_data.fuel_moment = 0.f;

    // ���������� ��������/�������� �������
    if (RTC_is_Time_passed(Fuel_update_time, 60))
    {
        update_fuel_flow(); // ��������� ������ ��� � ������
        Fuel_update_time = RTC_Get_timestamp();
    }

    // ���������� ���������� ������� � ������ �������
    update_ADC_val();

    // ���������� ��������� - ��� � 5 ���
    if (RTC_is_Time_passed(cnt_time_update_time, 5))
    {
        uint32_t dist_delta = cnt_dist; // ��������� ������� ��������
        cnt_dist = 0; // ���������� ����� ���� �� ������ ��������
        uint32_t fuel_delta = cnt_fuel;
        cnt_fuel = 0;

        dist_delta = (uint32_t) (((float) dist_delta) / config.speed_pulses);
        fuel_delta = (uint32_t) (((float) fuel_delta) / PULSES_PER_LITER * 1000.f);

        uint32_t time_delta = 0;
        if (val_data.speed > 1.f) // ���� �� ����� �� �����
        {
            time_delta = RTC_get_Time_passed(cnt_time_update_time); // ������� ������� ������� ������ � �������� ����������
        }
        cnt_time_update_time = RTC_Get_timestamp();

        for (uint8_t cn = 0; cn < 2; cn++)
        {
            val_data.cnt[cn].dist += dist_delta; // ����������
            val_data.cnt[cn].fuel += fuel_delta; // �������
            val_data.cnt[cn].time += time_delta; // �����
        }
    }

    // TODO: ����� ����������
}

uint32_t dist_cnt_get(void) // �������� �������� �������� �������� - ��� ����������
{
    return config_dist;
}

void dist_cnt_reset(void)
{
    config_dist = 0;
}

void update_ADC_val(void)
{
    static uint16_t Vin_ADC = 0;
    static uint16_t DUT_ADC = 0;
    if (LL_ADC_IsActiveFlag_JEOS(ADC1))
    {
        LL_ADC_ClearFlag_JEOS(ADC1);
        uint16_t Vref_ADC = LL_ADC_INJ_ReadConversionData12(ADC1, LL_ADC_INJ_RANK_1);
        Vin_ADC = LL_ADC_INJ_ReadConversionData12(ADC1, LL_ADC_INJ_RANK_2);
        DUT_ADC = LL_ADC_INJ_ReadConversionData12(ADC1, LL_ADC_INJ_RANK_3);

        LL_ADC_INJ_StartConversionSWStart(ADC1);

        float ADC_step = 1.2f / ((float) Vref_ADC);
        val_data.Vbat = (float) Vin_ADC * ADC_step * ((15.f + 3.9f) / 3.9f);

        val_data.fuel_level = config.DUT_cali_K * (float) DUT_ADC + config.DUT_cali_B; // TODO: ������� � ������ ���������� ������
        if (val_data.fuel_level < 0.f)
            val_data.fuel_level = 0.f;

        //TODO: ������
    }
}

uint16_t DUT_get_val(void)
{
    return LL_ADC_INJ_ReadConversionData12(ADC1, LL_ADC_INJ_RANK_3); // TODO: ������ � �����������
}

void DUT_calibrate(void) // ������������ ����������� ��� ���������� ���
{
    config.DUT_cali_K = (config.DUT_cali_Litres_1 - config.DUT_cali_Litres_2)
            / ((float) (config.DUT_cali_ADC_1 - config.DUT_cali_ADC_2));
    config.DUT_cali_B = config.DUT_cali_Litres_2 - config.DUT_cali_K * (float) config.DUT_cali_ADC_2;
}

void update_fuel_flow(void) // �������� ��� � ������
{
    uint16_t temp_dist = flow_dist;
    uint16_t temp_fuel = flow_fuel;
    flow_dist = 0;
    flow_fuel = 0;
    if (temp_dist != 0) // ������� ������� ������
        val_data.fuel_cur = (((float) temp_fuel) * (100.f * 1000.f) / PULSES_PER_LITER)
                / (((float) temp_dist) / config.speed_pulses);
    else
        val_data.fuel_cur = -1.f;

    int8_t cnt_temp = fuel_buffer_cnt;
    int8_t ptr_temp = (int8_t) fuel_buffer_ptr - (int8_t) fuel_buffer_cnt;
    if (ptr_temp < 0)
        ptr_temp += FUEL_BUFFER_LEN;

    uint32_t dist_sum = 0;
    uint32_t fuel_sum = 0;
    while (cnt_temp--) // ������ ����� ���� �������� � ������
    {
        dist_sum += fuel_buffer[ptr_temp][0];
        fuel_sum += fuel_buffer[ptr_temp][1];
        ptr_temp++;
        if (ptr_temp > (FUEL_BUFFER_LEN - 1))
            ptr_temp = 0;
    }
    if (dist_sum != 0) // ������� ������� ������
        val_data.fuel_avg = (((float) fuel_sum) * (100.f * 1000.f) / PULSES_PER_LITER)
                / (((float) dist_sum) / config.speed_pulses);
    else
        val_data.fuel_avg = -1.f;

    fuel_buffer[fuel_buffer_ptr][0] = temp_dist; // ���������� ������� �������� � �����
    fuel_buffer[fuel_buffer_ptr][1] = temp_fuel;
    fuel_buffer_ptr++;
    if (fuel_buffer_ptr > (FUEL_BUFFER_LEN - 1))
        fuel_buffer_ptr = 0;
    if (fuel_buffer_cnt < FUEL_BUFFER_LEN)
        fuel_buffer_cnt++;
    else
        fuel_buffer_cnt = FUEL_BUFFER_LEN;
}

void EXTI4_IRQHandler(void) // ���������� ������� � ������� ��������
{
    if (!LL_GPIO_IsInputPinSet(GPIOB, LL_GPIO_PIN_4))
    		//LL_EXTI_IsActiveFlag_0_31(LL_EXTI_LINE_4))
    {
		LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_4);
		int32_t tim_current = ((uint32_t) tim_cnt_high << 16UL) | LL_TIM_GetCounter(TIM3);
		int32_t speed_period_temp = (int32_t) tim_current - (int32_t) speed_tim_last;
		if (speed_period_temp < 0)
			speed_period_temp += 0x00FFFFFF;

		//if (speed_count_sum == 0)
			speed_period_sum = speed_period_temp;
		//else
		//    speed_period_sum += speed_period_temp;
		//speed_count_sum = 1;//++;
		speed_tim_last = tim_current;
		cnt_dist++;
		flow_dist++;
		config_dist++;

		speed_pulse_last = RTC_Get_timestamp();
    }
}

void EXTI3_IRQHandler(void) // ���������� ������� � �����������
{
	if (!LL_GPIO_IsInputPinSet(GPIOB, LL_GPIO_PIN_3))
	{
		LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_3);
		int32_t tim_current = ((uint32_t) tim_cnt_high << 16) | LL_TIM_GetCounter(TIM3);
		int32_t fuel_period_temp = (int32_t) tim_current - (int32_t) fuel_tim_last;
		if (fuel_period_temp < 0)
			fuel_period_temp += 0x00FFFFFF;
		fuel_period = fuel_period_temp;
		fuel_tim_last = tim_current;
		cnt_fuel++;
		flow_fuel++;

		fuel_pulse_last = RTC_Get_timestamp();
	}
}

void TIM3_IRQHandler(void)
{
    LL_TIM_ClearFlag_UPDATE(TIM3);
    tim_cnt_high++; // 16 ��� ������� ��� ����, ������� ��� 8 ������� ���������
}

void Speed_Fuel_Init(void)
{
    LL_EXTI_InitTypeDef EXTI_InitStruct;
    LL_GPIO_InitTypeDef GPIO_InitStruct;
    LL_TIM_InitTypeDef TIM_InitStruct;

    val_data.fuel_avg = -1.f;
    val_data.fuel_cur = -1.f;

    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM3); // ������ ��� ��������� ������� ��������

    TIM_InitStruct.Prescaler = 72 - 1; // 72000000/72 = 1 000 000 - 1���
    TIM_InitStruct.Autoreload = 0xFFFF;
    TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
    TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
    TIM_InitStruct.RepetitionCounter = 0;
    LL_TIM_Init(TIM3, &TIM_InitStruct);

    LL_TIM_EnableIT_UPDATE(TIM3);
    NVIC_SetPriority(TIM3_IRQn, 0);
    NVIC_EnableIRQ(TIM3_IRQn);

    LL_TIM_EnableCounter(TIM3);

    Fuel_update_time = RTC_Get_timestamp();
    cnt_time_update_time = RTC_Get_timestamp();

    GPIO_InitStruct.Pin = LL_GPIO_PIN_3 | LL_GPIO_PIN_4; // ������� ���������� �� �������� � ��������
    GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
    LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    LL_GPIO_AF_SetEXTISource(LL_GPIO_AF_EXTI_PORTB, LL_GPIO_AF_EXTI_LINE3);
    LL_GPIO_AF_SetEXTISource(LL_GPIO_AF_EXTI_PORTB, LL_GPIO_AF_EXTI_LINE4);

    NVIC_SetPriority(EXTI3_IRQn, 0);
    NVIC_EnableIRQ(EXTI3_IRQn);

    NVIC_SetPriority(EXTI4_IRQn, 0);
    NVIC_EnableIRQ(EXTI4_IRQn);

    EXTI_InitStruct.Line_0_31 = LL_EXTI_LINE_3 | LL_EXTI_LINE_4;
    EXTI_InitStruct.Mode = LL_EXTI_MODE_IT;
    EXTI_InitStruct.Trigger = LL_EXTI_TRIGGER_FALLING;
    EXTI_InitStruct.LineCommand = ENABLE;
    LL_EXTI_Init(&EXTI_InitStruct);
}

void ADC_Init(void)
{
    LL_GPIO_InitTypeDef GPIO_InitStruct;
    LL_ADC_InitTypeDef ADC_InitStruct;
    LL_ADC_INJ_InitTypeDef ADC_INJ_InitStruct;

    LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_ADC1);

    GPIO_InitStruct.Pin = LL_GPIO_PIN_1 | LL_GPIO_PIN_4;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_ANALOG;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
    LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    ADC_InitStruct.DataAlignment = LL_ADC_DATA_ALIGN_RIGHT;
    ADC_InitStruct.SequencersScanMode = LL_ADC_SEQ_SCAN_ENABLE;
    LL_ADC_Init(ADC1, &ADC_InitStruct);

    ADC_INJ_InitStruct.TriggerSource = LL_ADC_INJ_TRIG_SOFTWARE;
    ADC_INJ_InitStruct.SequencerLength = LL_ADC_INJ_SEQ_SCAN_ENABLE_3RANKS;
    ADC_INJ_InitStruct.SequencerDiscont = LL_ADC_INJ_SEQ_DISCONT_DISABLE;
    ADC_INJ_InitStruct.TrigAuto = LL_ADC_INJ_TRIG_INDEPENDENT;
    LL_ADC_INJ_Init(ADC1, &ADC_INJ_InitStruct);

    LL_ADC_SetCommonPathInternalCh(__LL_ADC_COMMON_INSTANCE(ADC1), LL_ADC_PATH_INTERNAL_VREFINT);

    LL_ADC_SetChannelSamplingTime(ADC1, LL_ADC_CHANNEL_VREFINT, LL_ADC_SAMPLINGTIME_239CYCLES_5);

    LL_ADC_INJ_SetSequencerRanks(ADC1, LL_ADC_INJ_RANK_1, LL_ADC_CHANNEL_VREFINT); // Vref
    LL_ADC_INJ_SetSequencerRanks(ADC1, LL_ADC_INJ_RANK_2, LL_ADC_CHANNEL_4); // Vin
    LL_ADC_INJ_SetSequencerRanks(ADC1, LL_ADC_INJ_RANK_3, LL_ADC_CHANNEL_1); // ���

    LL_ADC_Enable(ADC1);

    LL_ADC_StartCalibration(ADC1);
    while (LL_ADC_IsCalibrationOnGoing(ADC1))
        ;

    LL_ADC_INJ_StartConversionSWStart(ADC1);
}
