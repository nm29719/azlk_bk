#include "KL.h"
#include "string.h"

uint32_t KL_last_active = 0;

kl_data_t KL_data;

uint8_t KL_Rx_buffer[KL_BUFFERS][KL_BUFFER_LEN]; // ����� ������� ������
uint8_t * KL_Rx_data = NULL; // ��������� �� ������ ������� ��������������� ������
uint8_t KL_Rx_bytecnt = 0; // ���������� ���� � ������� ������
uint8_t KL_Rx_current = 0; // ����� �������� ������������ ������ � ������
uint8_t KL_Rx_last = 0; // ����� ���������� ��������� ������ � ������
uint8_t KL_Rx_cnt = 0; // ���������� �������������� �������
uint8_t KL_Tx_last[8]; // ��������� ���������� �����
uint16_t KL_Init_data = 0;
uint8_t KL_Init_cnt = 0;
uint8_t KL_state = KL_DISCONNECTED; // ������� ���������

const uint8_t KL_request[][2] = // �������
{
		{0x36}, 		// Tester present
		{0x31, 0x52}	// Controle parametres
};

void KL_send_Packet(uint8_t * data, uint8_t len);
uint8_t KL_Parse_Packet(uint8_t * data);
void KL_Parse_3152(uint8_t * data);
uint8_t KL_Get_response(void);
static void KL_reset_buffer(void);

uint8_t KL_main_proc(void)
{
	static uint8_t KL_active = 0;
	if (KL_state == KL_DISCONNECTED) // ��������� - ���� �� ����������� ��� ��������� ������
	{
		KL_reset_buffer();
		KL_state = KL_INIT_SENT; // ���������� ���� ��������� ���, UART ��� �� �����
		LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_9, LL_GPIO_MODE_OUTPUT); // ����������� ��� � ����� GPIO
		LL_USART_DisableIT_RXNE(USART1); // �������� ���������� ���� �� ������
		KL_Init_data = 0x05E9; //01011101001
		KL_Init_cnt = 10; // 10���
		LL_TIM_EnableCounter(TIM4); // ������� ������
		KL_last_active = RTC_Get_timestamp();
	}
	if (KL_state == KL_INIT_SENT) // ���� �������� �����
	{
		if (KL_Init_cnt == 0) // ���� �������� ����������� ��� ������
		{
			KL_state = KL_INIT_WAIT; // ������� UART ������� � ���� ������
			LL_USART_ClearFlag_RXNE(USART1);
			LL_USART_EnableIT_RXNE(USART1);
			LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_9, LL_GPIO_MODE_ALTERNATE);
			KL_last_active = RTC_Get_timestamp();
		}
		else if (RTC_is_Time_passed(KL_last_active, 5))
		{
			KL_state = KL_ERROR; // ���� �������� �������
			KL_last_active = RTC_Get_timestamp();
		}
	}
	if (KL_state == KL_INIT_WAIT) // ���� ���������, ���� ������
	{
		if (KL_Get_response()) // ��������� �����
		{
			if ((KL_Rx_data[0] == 0x55) && (KL_Rx_data[1] == 0x0B) && (KL_Rx_data[2] == 0x85))
			{
				KL_state = KL_IDLE; // ��� ��, ��������� � ������� �����
				KL_last_active = RTC_Get_timestamp();
			}
			else
			{
				KL_state = KL_ERROR; // ������ �� ��
				KL_last_active = RTC_Get_timestamp();
			}
		}
		else if (RTC_is_Time_passed(KL_last_active, 5))
		{
			KL_state = KL_ERROR; // �� 5 ��� ������ �� ������
			KL_last_active = RTC_Get_timestamp();
		}
	}

	if (KL_state == KL_IDLE) // ����������, ����� ���������� �������
	{
		if (RTC_is_Time_passed(KL_last_active, 1))
		{
			KL_send_Packet((uint8_t *)KL_request[1], 2);
			KL_state = KL_REQUEST_SENT;
			KL_last_active = RTC_Get_timestamp();
		}
	}

	if (KL_state == KL_REQUEST_SENT) // ��������� �������� �� �� �� ��� ���������
	{
		if (KL_Get_response()) // ������������ ������� ������ ������ �������
		{
			if (memcmp(KL_Rx_data, KL_Tx_last, KL_Tx_last[0]+1) == 0)
			{
				KL_state = KL_REQUEST_WAIT; // ��� ��, ���� ������
				KL_last_active = RTC_Get_timestamp();
			}
			else
			{
				KL_state = KL_ERROR; // ������� ������ �� ��
				KL_last_active = RTC_Get_timestamp();
			}
		}
		else if (RTC_is_Time_passed(KL_last_active, 1))
		{
			KL_state = KL_ERROR; // ������ ������ �� ������
			KL_last_active = RTC_Get_timestamp();
		}
	}

	if (KL_state == KL_REQUEST_WAIT) // ���� ������
	{
		if (KL_Get_response())
		{
			if (KL_Parse_Packet(KL_Rx_data) == 1) // ��������� �������� �����, ��������� ����������� �����
			{
				KL_active = 1;
				KL_state = KL_IDLE; // ��� ��, ������������ � ���������� ���������
				KL_last_active = RTC_Get_timestamp();
			}
			else
			{
				KL_active = 0;
				KL_state = KL_ERROR; // �� ��
				KL_last_active = RTC_Get_timestamp();
			}
		}
		else if (RTC_is_Time_passed(KL_last_active, 5))
		{
			KL_state = KL_ERROR; // ��� ������ � �� ������
			KL_last_active = RTC_Get_timestamp();
		}
	}

	if (KL_state == KL_ERROR) // �����
	{
		KL_active = 0;
		if (RTC_is_Time_passed(KL_last_active, 10)) // ���� 10��� � ������� �����
		{
			KL_data.err_cnt++;
			KL_state = KL_DISCONNECTED;
		}
	}
	return KL_active;
}

void KL_send_Packet(uint8_t * data, uint8_t len)
{
	uint8_t checksum = 0;
	KL_Tx_last[0] = len+1; // ��������� ����� � ����������� �����
	checksum = KL_Tx_last[0];
	for (uint8_t i = 0; i < len; i++)
	{
		KL_Tx_last[i+1] = data[i];
		checksum += data[i];
	}
	KL_Tx_last[len+1] = checksum;

	KL_Rx_bytecnt = 0; // �� ������ ������

	for (uint8_t i = 0; i < len+2; i++) // ����������
	{
		while(!(USART1->SR & USART_SR_TXE));
		USART1->DR = KL_Tx_last[i];
	}
}

uint8_t KL_Parse_Packet(uint8_t * data)
{
	uint8_t checksum = 0;
	uint8_t len = data[0] - 1; // ��������� ����������� �����
	for (uint8_t i = 0; i < len+1; i++)
	{
		checksum += data[i];
	}
	if (checksum != data[len+1])
		return 0;

//	if ((len == 1) && (data[1] == 0x36)) // ���������� ��� ������
//		return 1;

	if ((len == 36) && (data[1] == 0x31) && (data[2] == 0x52))
		KL_Parse_3152(data);
	return 1;
}

void KL_Parse_3152(uint8_t * data) // ��������� ����� Controle parametres
{
	KL_data.RPM = (data[8] << 8) | (data[9] & 0xFF);
	KL_data.tr_pos = data[14];
	KL_data.T_w = ((float)data[16])*0.625f - 40.f;
	KL_data.T_a = ((float)data[17])*0.625f - 40.f;
	KL_data.Vbat = ((float)data[18])*0.0312f + 8.f;
	KL_data.speed = data[19];
}

uint8_t KL_Get_response(void) // �������� ����� �� ������
{
	int8_t Rx_curr = 0;
	if (KL_Rx_cnt == 0) // ������� ���
	{
		return 0;
	}
	if (KL_Rx_cnt > (KL_BUFFERS-1))
		KL_Rx_cnt = KL_BUFFERS-1;

	Rx_curr = KL_Rx_last+1-KL_Rx_cnt;
	if (Rx_curr < 0)
		Rx_curr += KL_BUFFERS; // ������� ������ �������������
	KL_Rx_cnt--;

	KL_Rx_data = KL_Rx_buffer[Rx_curr];
	return 1;
}

static void KL_reset_buffer(void)
{
	KL_Rx_bytecnt = 0;
	KL_Rx_current = 0;
	KL_Rx_last = 0;
	KL_Rx_cnt = 0;
}

void KL_HW_Init(void)
{
	LL_USART_InitTypeDef USART_InitStruct;
	LL_GPIO_InitTypeDef GPIO_InitStruct;
	LL_TIM_InitTypeDef TIM_InitStruct;

	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM4);

	TIM_InitStruct.Prescaler = 36000-1; // 72000000/36000/400 = 5��
	TIM_InitStruct.Autoreload = 400-1;
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
	TIM_InitStruct.RepetitionCounter = 0;
	LL_TIM_Init(TIM4,&TIM_InitStruct);

	LL_TIM_DisableCounter(TIM4);

	LL_TIM_EnableIT_UPDATE(TIM4);
	NVIC_SetPriority(TIM4_IRQn,0);
	NVIC_EnableIRQ(TIM4_IRQn);

	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_USART1);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_9; // Tx
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_9);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_10; // Rx
	GPIO_InitStruct.Mode = LL_GPIO_MODE_FLOATING;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	USART_InitStruct.BaudRate = 10400;
	USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
	USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
	USART_InitStruct.Parity = LL_USART_PARITY_NONE;
	USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
	USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
	LL_USART_Init(USART1, &USART_InitStruct);

	LL_USART_EnableIT_RXNE(USART1);
	LL_USART_ConfigAsyncMode(USART1);
	LL_USART_Enable(USART1);

	NVIC_SetPriority(USART1_IRQn, 0);
	NVIC_EnableIRQ(USART1_IRQn);
}


void USART1_IRQHandler(void)
{
	uint8_t frameend = 0;
	if (LL_USART_IsActiveFlag_RXNE(USART1))
	{
		LL_USART_ClearFlag_RXNE(USART1);
		uint8_t data = LL_USART_ReceiveData8(USART1);
		KL_Rx_buffer[KL_Rx_current][KL_Rx_bytecnt++] = data;
		if (KL_Rx_bytecnt > KL_BUFFER_LEN)
			KL_Rx_bytecnt = 0;
		if ((KL_Rx_bytecnt > 2) && (KL_Rx_bytecnt > KL_Rx_buffer[KL_Rx_current][0]))
			frameend = 1;
		if ((KL_state == KL_INIT_WAIT) && (KL_Rx_bytecnt == 3)) // �������� ����� �� ����
			frameend = 1;
	}
	if (LL_USART_IsActiveFlag_ORE(USART1))
		LL_USART_ClearFlag_ORE(USART1); // �� ������ ������

	if (frameend == 1)
	{
		KL_Rx_cnt++;
		KL_Rx_last = KL_Rx_current;
		KL_Rx_current++;

		if (KL_Rx_current >= KL_BUFFERS)
			KL_Rx_current = 0;
		KL_Rx_bytecnt = 0;
	}
}

void TIM4_IRQHandler(void)
{
	if (LL_TIM_IsActiveFlag_UPDATE(TIM4))
	{
		LL_TIM_ClearFlag_UPDATE(TIM4);
		if (KL_Init_cnt != 0)
		{
			if ((KL_Init_data & 0x01) != ((GPIOA->IDR >> 10) & 0x01))
			{
				LL_TIM_DisableCounter(TIM4);
				KL_state = KL_ERROR; // �� ����� �� �� ��� ���������� ������� ���
				KL_Init_cnt = 0;
				return;
			}

			KL_Init_data >>= 1;
			if (KL_Init_data & 0x01)
				LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_9);
			else
				LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_9);

			KL_Init_cnt--;
		}
		else
		{
			LL_TIM_DisableCounter(TIM4);
		}

	}
}
