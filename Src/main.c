#include "main.h"
#include "printf.h"
#include "KL.h"
#include "speed_fuel.h"

#include <math.h>

void update_clock(void);
void update_KL_data(void);
void Clock_Init(void);
void screen_update(void);
void screen_draw(uint8_t screen);

void draw_footer(uint8_t screen);

void update_logo(void);
void update_main(void);
void update_fuel(void);
void update_cnt(uint8_t n);
void update_errors(void);
void update_menu(void);
void update_settime(void);
void update_cfgspeed(void);
void update_cfgdut(void);

void Pwr_Off(void);

void MISC_Init(void);
void button_Init(void);
void UART_Init(void);

void Config_write(void);
void Config_read(void);
void Config_default(void);

extern kl_data_t KL_data;
extern vals_data_t val_data;

uint32_t time_last = 0;

uint8_t scr = SCREEN_LOGO;
uint8_t screen_last = SCREEN_LOGO;
uint8_t pwr_state = 0;

uint8_t KL_active = 0;

uint8_t * config_ptr = NULL;

uint8_t button_pressed = BUTTON_NONE;
uint8_t menu_sel = 0;

uint32_t dist_cnt_start = 0;
float speed_pulses_set = 0.f;

time_struct time_set;
Config_type config;
DUT_cali_point_type DUT_cali[2];

/* �������:
 * 1 - �������?
 * 2 - ��������� ������
 * 3 - ��������� ������� ���������
 * 4 - KL
 */

/*TODO:
 * 0. ���������� ��������
 * 1. ����� �������
 * 2. ����� ������
 * 3. ���������� � backup ram
 * 4. �������
 * 5. ��������� � ����� ��������
 * 6. ���������� ���������� �������� ���
 * 7. ������
 * 8. ��������� RTC
 *
 */
int main(void)
{
    uint8_t IGN_last = 0;
    uint32_t IGN_OFF_time = 0;

    Clock_Init();
    Config_read();
    //UART_Init();
    LCD_init();
    MISC_Init();
    KEY_BL_OFF;

    if (LL_GPIO_IsInputPinSet(GPIOA, LL_GPIO_PIN_0))
        Pwr_Off();

    screen_draw(SCREEN_LOGO);
    LL_mDelay(10);
    LCD_BL_ON;

    RTC_Init();
    time_last = RTC_Get_timestamp();

    button_Init();
    KL_HW_Init();
    Speed_Fuel_Init();

    ADC_Init();

    while (1)
    {
        Speed_Fuel_proc();
        KL_active = KL_main_proc(); // ���������� ���������� � �������
        screen_update(); // ��������� �����
        if (LL_GPIO_IsInputPinSet(GPIOA, LL_GPIO_PIN_0)) // ���� ��������� ���������
        {
            if (IGN_last == 1)
            {
                IGN_OFF_time = RTC_Get_timestamp();
            }
            if (RTC_is_Time_passed(IGN_OFF_time, 2))
            {
                Config_write();
                Pwr_Off();
            }
            IGN_last = 0;
        }
        else
        {
            IGN_last = 1;
        }
    }
}

void screen_draw(uint8_t screen)
{
    switch (screen)
    {
    case SCREEN_LOGO:
        LCD_SetBGColor(BLACK);
        LCD_SetTextColor(RED);
        LCD_Image_mono(Logo, 0, 0);
        break;
    case SCREEN_MAIN:
    case SCREEN_FUEL:
    case SCREEN_CNTA:
    case SCREEN_CNTB:
        LCD_Fill(0, 58, 240, 240 - 58 - 14, BLACK);
        LCD_Fill(0, 0, 240, 58, GRAY);
        LCD_Fill(0, 226, 240, 14, GRAY);
        draw_footer(screen);
        LCD_Fill(0, 142, 240, 1, WHITE);
        LCD_Fill(120, 58, 1, 168, WHITE);
        screen_last = screen;
        break;
    case SCREEN_ERRORS:
        LCD_Fill(0, 58, 240, 240 - 58 - 14, BLACK);
        LCD_Fill(0, 0, 240, 58, GRAY);
        LCD_Fill(0, 226, 240, 14, GRAY);
        draw_footer(screen);
        screen_last = screen;
        break;
    case SCREEN_MENU:
        menu_sel = 0;
        LCD_Fill(0, 0, 240, 240, BLACK);
        break;
    case SCREEN_SETTIME:
        RTC_Get_Time(&time_set);
        menu_sel = 0;
        LCD_Fill(0, 0, 240, 240, BLACK);
        break;
    case SCREEN_CFGSPEED:
        dist_cnt_start = dist_cnt_get();
        speed_pulses_set = config.speed_pulses;
        menu_sel = 0;
        LCD_Fill(0, 0, 240, 240, BLACK);
        break;
    case SCREEN_CFGDUT:
        DUT_cali[0].ADC_val = config.DUT_cali_ADC_1;
        DUT_cali[1].ADC_val = config.DUT_cali_ADC_2;
        DUT_cali[0].Litres = config.DUT_cali_Litres_1;
        DUT_cali[1].Litres = config.DUT_cali_Litres_2;
        menu_sel = 0;
        LCD_Fill(0, 0, 240, 240, BLACK);
        break;
    default:
        break;
    }
    scr = screen;
}

void screen_update(void)
{
    switch (scr)
    {
    case SCREEN_LOGO:
        update_logo();
        break;
    case SCREEN_MAIN:
        update_clock();
        update_main();
        break;
    case SCREEN_FUEL:
        update_clock();
        update_fuel();
        break;
    case SCREEN_CNTA:
        update_clock();
        update_cnt(0);
        break;
    case SCREEN_CNTB:
        update_clock();
        update_cnt(1);
        break;
    case SCREEN_ERRORS:
        update_clock();
        update_errors();
        break;
    case SCREEN_MENU:
        update_menu();
        break;
    case SCREEN_SETTIME:
        update_settime();
        break;
    case SCREEN_CFGSPEED:
        update_cfgspeed();
        break;
    case SCREEN_CFGDUT:
        update_cfgdut();
        break;
    default:
        break;
    }
}

void update_logo(void)
{
    if ((scr == SCREEN_LOGO) && (RTC_is_Time_passed(time_last, 2)))
    {
        screen_draw(SCREEN_MAIN);
        KEY_BL_ON;
    }
}

void update_main(void)
{
    // ���������� ������
    LCD_SetBGColor(BLACK);
    LCD_SetTextColor(GREEN);
    LCD_SetFont(&t_12x24_full);

    LCD_SetTextPos(4, 58);
    LCD_printf("�������");
    LCD_SetTextPos(125, 58);
    LCD_printf("��������");
    LCD_SetTextPos(4, 143);
    LCD_printf("����");
    LCD_SetTextPos(125, 143);
    LCD_printf("������.");

    LCD_SetBGColor(BLACK);
    LCD_SetTextColor(WHITE);
    LCD_SetFont(&t_16x22_digits);

    LCD_SetTextPos(132, 98);
    if (val_data.speed < 300.f) // ��������
        LCD_printf("%4.0f", val_data.speed);
    else
        LCD_printf("----");

//	LCD_SetTextPos(132,183);
//	LCD_printf("%4.1f",Vin); // �����

    if (KL_active)
    {
        LCD_SetTextPos(11, 98);
        LCD_printf("%4u", KL_data.RPM); // �������
        LCD_SetTextPos(11, 183);
        if (KL_data.T_w < 100.f) // ���
            LCD_printf("%4.1f", KL_data.T_w);
        else
            LCD_printf("%4.0f", KL_data.T_w);
        LCD_SetTextPos(132, 183);
        LCD_printf("%4.1f", KL_data.Vbat); // �����
    }
    else // ��� ������ �� ������
    {
        LCD_SetTextColor(RED);
        LCD_SetTextPos(11, 98);
        LCD_printf("----");
        LCD_SetTextPos(11, 183);
        LCD_printf("----");
        LCD_SetTextPos(132, 183);
        LCD_printf("----");
    }

    LCD_SetBGColor(BLACK);
    LCD_SetTextColor(WHITE);
    LCD_SetFont(&symbols);

    LCD_SetTextPos(81, 93);
    LCD_putchar(0); // ��/���

    LCD_SetTextPos(202, 93);
    LCD_putchar(1); // ��/�

    LCD_SetTextPos(81, 178);
    LCD_putchar(6); // �

    LCD_SetTextPos(202, 178);
    LCD_putchar(5); // �

    // ���������� ������
    switch (button_pressed)
    {
    case BUTTON_DN:
        screen_draw(SCREEN_FUEL);
        break;
    case BUTTON_OK:
        screen_draw(SCREEN_MENU);
        break;
    default:
        break;
    }
    button_pressed = BUTTON_NONE;
}

void update_fuel(void)
{
    // ���������� ������
    LCD_SetBGColor(BLACK);
    LCD_SetTextColor(GREEN);
    LCD_SetFont(&t_12x24_full);

    LCD_SetTextPos(4, 58);
    LCD_printf("�������");
    LCD_SetTextPos(125, 58);
    LCD_printf("�������");
    LCD_SetTextPos(4, 143);
    LCD_printf("������.");
    LCD_SetTextPos(125, 143);
    LCD_printf("����");

    LCD_SetBGColor(BLACK);
    LCD_SetTextColor(WHITE);
    LCD_SetFont(&t_16x22_digits);

    LCD_SetTextPos(11, 98); // ������� ������
    if (val_data.speed <= 2.f)
    {
    	LCD_printf("----");
    }
    else
    {
        float fuel_temp = val_data.fuel_moment / val_data.speed * 100.f;
        if ((fuel_temp < 0.f) || (fuel_temp > 999.f))
        {
            LCD_printf("----");
        }
        else
        {
            if (fuel_temp < 100.f)
                LCD_printf("%4.1f", fuel_temp);
            else
                LCD_printf("%4.0f", fuel_temp);
        }
    }

    LCD_SetTextPos(132, 98); // ������� ������
    if ((val_data.fuel_avg < 0.f) || (val_data.fuel_avg > 999.f))
    {
        LCD_printf("----");
    }
    else
    {
        if (val_data.fuel_avg < 100.f)
            LCD_printf("%4.1f", val_data.fuel_avg);
        else
            LCD_printf("%4.0f", val_data.fuel_avg);
    }

    LCD_SetTextPos(11, 183); // ������������ ������
    if (val_data.fuel_moment < 100.f)
        LCD_printf("%4.1f", val_data.fuel_moment);
    else
        LCD_printf("%4.0f", val_data.fuel_moment);


    if (KL_active)
    {
    	LCD_SetTextPos(132, 183);
        if (KL_data.T_w < 100.f) // ���
            LCD_printf("%4.1f", KL_data.T_w);
        else
            LCD_printf("%4.0f", KL_data.T_w);
    }
    else // ��� ������ �� ������
    {
        LCD_SetTextColor(RED);
        LCD_SetTextPos(132, 183);
        LCD_printf("----");
    }

//    LCD_SetTextPos(132, 183); // ������� � ����
//    if (!isnanf(val_data.fuel_level))
//    {
//        LCD_SetTextColor(WHITE);
//        LCD_printf("%4.1f", 0.f);//val_data.fuel_level);
//    }
//    else
//    {
//        LCD_SetTextColor(RED);
//        LCD_printf("----");
//    }

    LCD_SetBGColor(BLACK);
    LCD_SetTextColor(WHITE);
    LCD_SetFont(&symbols);

    LCD_SetTextPos(81, 93);
    LCD_putchar(2); // �/100

    LCD_SetTextPos(202, 93);
    LCD_putchar(2); // �/100

    LCD_SetTextPos(81, 178);
    LCD_putchar(3); // �/�

    LCD_SetTextPos(202, 178);
    LCD_putchar(6); // �

    // ���������� ������
    switch (button_pressed)
    {
    case BUTTON_UP:
        screen_draw(SCREEN_MAIN);
        break;
    case BUTTON_DN:
        screen_draw(SCREEN_CNTA);
        break;
    case BUTTON_OK:
        screen_draw(SCREEN_MENU);
        break;
    default:
        break;
    }
    button_pressed = BUTTON_NONE;
}

void update_cnt(uint8_t n)
{
    float dist = val_data.cnt[n].dist / 1000.f;
    float fuel = val_data.cnt[n].fuel / 1000.f;
    uint32_t time = val_data.cnt[n].time / 60;
    // ���������� ������
    LCD_SetBGColor(BLACK);
    LCD_SetTextColor(GREEN);
    LCD_SetFont(&t_12x24_full);

    LCD_SetTextPos(4, 58);
    LCD_printf("������");
    LCD_SetTextColor(WHITE);
    LCD_printf(" ��");

    LCD_SetTextPos(125, 58);
    LCD_SetTextColor(GREEN);
    LCD_printf("������");
    LCD_SetTextColor(WHITE);
    LCD_printf("  �");

    LCD_SetTextColor(GREEN);
    LCD_SetTextPos(4, 143);
    LCD_printf("�����");

    LCD_SetTextPos(125, 143);
    LCD_printf("����. ��.");

    LCD_SetBGColor(BLACK);
    LCD_SetTextColor(WHITE);
    LCD_SetFont(&t_16x22_digits);

    LCD_SetTextPos(11, 98); // ������
    if (dist < 1000.f)
        LCD_printf("%6.1f", dist); // < 1000��
    else
        LCD_printf("%6.0f", dist); // > 1000��

    LCD_SetTextPos(132, 98); // ������
    if (fuel < 100.f)
        LCD_printf("%6.2f", fuel); // < 1000�
    else
        LCD_printf("%6.1f", fuel); // > 1000�

    LCD_SetTextPos(1, 183); // ����� � ����
    LCD_printf("%3u:%02u", time / 60, time % 60);

    LCD_SetTextPos(132, 183); // ������� ������ �� �������
    float fuel_temp = (fuel * 100.f) / dist;
    if ((dist > 0.f) && (fuel_temp < 100.f))
    {
        LCD_printf("%4.1f", fuel_temp);
    }
    else
        LCD_printf("----");

    LCD_SetBGColor(BLACK);
    LCD_SetTextColor(WHITE);
    LCD_SetFont(&symbols);

    LCD_SetTextPos(202, 178);
    LCD_putchar(2); // �/100

    // ���������� ������
    switch (button_pressed)
    {
    case BUTTON_UP:
        if (n == 0)
            screen_draw(SCREEN_FUEL);
        else if (n == 1)
            screen_draw(SCREEN_CNTA);
        break;
    case BUTTON_DN:
        if (n == 0)
            screen_draw(SCREEN_CNTB);
        else if (n == 1)
            screen_draw(SCREEN_ERRORS);
        break;
    case BUTTON_OK:
        screen_draw(SCREEN_MENU);
        break;
    case BUTTON_OK_LONG: // TODO: ��������, ����� �� ��������
        val_data.cnt[n].dist = 0;
        val_data.cnt[n].fuel = 0;
        val_data.cnt[n].time = 0;
        break;
    default:
        break;
    }
    button_pressed = BUTTON_NONE;
}

void update_errors(void)
{
    LCD_SetTextColor(WHITE);
    LCD_SetFont(&t_12x24_full);
    LCD_SetTextPos(4, 62);
    if (KL_active == 0)
    {
        LCD_SetBGColor(RED);
        LCD_printf(" ��� ����� � ���! ");
    }
    else
    {
        LCD_SetBGColor(0x03E0);
        LCD_printf("    ����������    ");
    }

    LCD_SetTextPos(4, 92);
    LCD_SetBGColor(BLACK);
    LCD_printf("PIZDETS_COUNT:%u", KL_data.err_cnt);

    switch (button_pressed)
    {
    case BUTTON_UP:
        screen_draw(SCREEN_CNTB);
        break;
    case BUTTON_OK:
        screen_draw(SCREEN_MENU);
        break;
    default:
        break;
    }
    button_pressed = BUTTON_NONE;
}

void update_menu(void) // ������� ����
{
    LCD_SetTextColor(WHITE);
    LCD_SetFont(&t_12x24_full);

    LCD_SetTextPos(20, 20);
    if (menu_sel == 0)
        LCD_SetBGColor(BLUE);
    else
        LCD_SetBGColor(BLACK);
    LCD_printf(" < ����� < ");

    LCD_SetTextPos(4, 60);
    if (menu_sel == 1)
        LCD_SetBGColor(BLUE);
    else
        LCD_SetBGColor(BLACK);
    LCD_printf(" ���. �����\n");
    if (menu_sel == 2)
        LCD_SetBGColor(BLUE);
    else
        LCD_SetBGColor(BLACK);
    LCD_printf(" ���������� ���\n");
    if (menu_sel == 3)
        LCD_SetBGColor(BLUE);
    else
        LCD_SetBGColor(BLACK);
    LCD_printf(" �����. �. ��������");
    if (menu_sel == 4)
        LCD_SetBGColor(BLUE);
    else
        LCD_SetBGColor(BLACK);
    LCD_printf(" Bootloader ");

    // TODO: ����� �� ���� �� ��������
    uint8_t ok_pressed = 0;
    switch (button_pressed)
    {
    case BUTTON_UP:
        if (menu_sel > 0)
            menu_sel--;
        break;
    case BUTTON_DN:
        if (menu_sel < 4)
            menu_sel++;
        break;
    case BUTTON_OK:
        ok_pressed = 1;
        break;
    default:
        break;
    }
    button_pressed = BUTTON_NONE;

    if (ok_pressed == 1)
    {
        switch (menu_sel)
        {
        case 0:
            screen_draw(screen_last);
            break;
        case 1:
            screen_draw(SCREEN_SETTIME);
            break;
        case 2:
            screen_draw(SCREEN_CFGDUT);
            break;
        case 3:
            screen_draw(SCREEN_CFGSPEED);
            break;
        case 4:
            RTC_Set_BL_flag();
            LCD_BL_OFF;
            NVIC_SystemReset();
            break;
        default:
            break;
        }
    }
}

void update_settime(void) // ���� ��������� �������
{
    static uint8_t time_edit = 0;
    LCD_SetTextColor(WHITE);
    LCD_SetFont(&t_12x24_full);

    LCD_SetTextPos(20, 20);
    if (menu_sel == 0)
        LCD_SetBGColor(BLUE);
    else
        LCD_SetBGColor(BLACK);
    LCD_printf(" < ����� < ");

    LCD_SetTextPos(70, 120);
    if (menu_sel == 3)
        LCD_SetBGColor(BLUE);
    else
        LCD_SetBGColor(BLACK);
    LCD_printf(" �� ");

    LCD_SetFont(&t_16x22_digits);
    LCD_SetTextPos(60, 80);
    if (menu_sel == 1)
        LCD_SetBGColor((time_edit == 0) ? BLUE : RED);
    else
        LCD_SetBGColor(BLACK);
    LCD_printf("%02d", time_set.hour);

    LCD_SetBGColor(BLACK);
    LCD_printf(":");

    if (menu_sel == 2)
        LCD_SetBGColor((time_edit == 0) ? BLUE : RED);
    else
        LCD_SetBGColor(BLACK);
    LCD_printf("%02d", time_set.min);

    uint8_t ok_pressed = 0;
    switch (button_pressed)
    {
    case BUTTON_UP:
        if ((time_edit == 0) && (menu_sel > 0))
            menu_sel--;
        else if ((time_edit == 1) && (time_set.hour < 23))
            time_set.hour++;
        else if ((time_edit == 2) && (time_set.min < 59))
            time_set.min++;
        break;
    case BUTTON_DN:
        if ((time_edit == 0) && (menu_sel < 4))
            menu_sel++;
        else if ((time_edit == 1) && (time_set.hour > 0))
            time_set.hour--;
        else if ((time_edit == 2) && (time_set.min > 0))
            time_set.min--;
        break;
    case BUTTON_OK:
        ok_pressed = 1;
        break;
    default:
        break;
    }
    button_pressed = BUTTON_NONE;

    if (ok_pressed == 1)
    {
        switch (menu_sel)
        {
        case 0:
            screen_draw(SCREEN_MENU);
            break;
        case 1:
            if (time_edit == 0)
                time_edit = 1;
            else
            {
                time_edit++; // ��������� �� ������
                menu_sel++;
            }
            break;
        case 2:
            if (time_edit == 0)
                time_edit = 2;
            else
                time_edit = 0;
            break;
        case 3:
            time_set.sec = 0;
            RTC_Set_Time(&time_set);
            screen_draw(SCREEN_MENU);
            break;
        default:
            break;
        }
    }
}

void update_cfgspeed(void) // ��������� ������� ��������
{
    static uint8_t val_edit = 0;
    uint32_t cnt_temp = dist_cnt_get() - dist_cnt_start;

    LCD_SetTextColor(WHITE);
    LCD_SetFont(&t_12x24_full);

    LCD_SetTextPos(20, 20);
    if (menu_sel == 0)
        LCD_SetBGColor(BLUE);
    else
        LCD_SetBGColor(BLACK);
    LCD_printf(" < ����� < ");

    LCD_SetTextPos(20, 90);
    if (menu_sel == 1)
        LCD_SetBGColor(BLUE);
    else
        LCD_SetBGColor(BLACK);
    LCD_printf(" �������� ");

    LCD_SetTextPos(20, 200); //150
    if (menu_sel == 3)
        LCD_SetBGColor(BLUE);
    else
        LCD_SetBGColor(BLACK);
    LCD_printf(" �� ");

    LCD_SetFont(&t_16x22_digits);
    LCD_SetTextPos(30, 150); //125
    if (menu_sel == 2)
        LCD_SetBGColor((val_edit == 0) ? BLUE : RED);
    else
        LCD_SetBGColor(BLACK);
    LCD_printf("%4.1f", speed_pulses_set);

    LCD_SetTextPos(30, 65);
    LCD_SetBGColor(BLACK);
    LCD_printf("%04d", cnt_temp);

    LCD_SetTextPos(30, 120);
    LCD_printf("%3.0f ", val_data.speed_cali);

    LCD_SetTextPos(120, 120);
    LCD_printf("%3.0f ", val_data.speed);

    uint8_t ok_pressed = 0;
    switch (button_pressed)
    {
    case BUTTON_UP:
        if ((val_edit == 0) && (menu_sel > 0))
            menu_sel--;
        else if ((val_edit == 1) && (speed_pulses_set < 20.0f))
            speed_pulses_set += 0.1f;
        break;
    case BUTTON_DN:
        if ((val_edit == 0) && (menu_sel < 3))
            menu_sel++;
        else if ((val_edit == 1) && (speed_pulses_set > 0.f))
            speed_pulses_set -= 0.1f;
        break;
    case BUTTON_OK:
        ok_pressed = 1;
        break;
    default:
        break;
    }
    button_pressed = BUTTON_NONE;
    val_data.speed_cali_coef = speed_pulses_set;

    if (ok_pressed == 1)
    {
        switch (menu_sel)
        {
        case 0:
            screen_draw(SCREEN_MENU);
            break;
        case 1:
            dist_cnt_start = dist_cnt_get();
            break;
        case 2:
            if (val_edit == 0)
                val_edit = 1;
            else
                val_edit = 0;
            break;
        case 3:
            config.speed_pulses = speed_pulses_set;
            screen_draw(SCREEN_MENU);
            break;
        default:
            break;
        }
    }
}

void update_cfgdut(void) // ���������� ���
{
    static uint8_t val_edit = 0;

    LCD_SetTextColor(WHITE);
    LCD_SetFont(&t_12x24_full);

    LCD_SetTextPos(20, 20);
    if (menu_sel == 0)
        LCD_SetBGColor(BLUE);
    else
        LCD_SetBGColor(BLACK);
    LCD_printf(" < ����� < ");

    LCD_SetTextPos(10, 60);
    LCD_SetBGColor(BLACK);
    LCD_printf("1: ");
    if (menu_sel == 1)
        LCD_SetBGColor(BLUE);
    else
        LCD_SetBGColor(BLACK);
    LCD_printf(" %4.1f � ", DUT_cali[0].Litres);
    LCD_SetBGColor(BLACK);
    LCD_printf(" - ");
    if (menu_sel == 2)
        LCD_SetBGColor(BLUE);
    else
        LCD_SetBGColor(BLACK);
    LCD_printf(" %03X ", DUT_cali[0].ADC_val);

    LCD_SetTextPos(10, 100);
    LCD_SetBGColor(BLACK);
    LCD_printf("2: ");
    if (menu_sel == 3)
        LCD_SetBGColor(BLUE);
    else
        LCD_SetBGColor(BLACK);
    LCD_printf(" %4.1f � ", DUT_cali[1].Litres);
    LCD_SetBGColor(BLACK);
    LCD_printf(" - ");
    if (menu_sel == 4)
        LCD_SetBGColor(BLUE);
    else
        LCD_SetBGColor(BLACK);
    LCD_printf(" %03X ", DUT_cali[1].ADC_val);

    LCD_SetTextPos(20, 150);
    if (menu_sel == 5)
        LCD_SetBGColor(BLUE);
    else
        LCD_SetBGColor(BLACK);
    LCD_printf(" �� ");

    uint8_t ok_pressed = 0;
    switch (button_pressed)
    {
    case BUTTON_UP:
        if ((val_edit == 0) && (menu_sel > 0))
            menu_sel--;
        else if ((val_edit == 1) && (DUT_cali[0].Litres < 80.0f))
            DUT_cali[0].Litres += 0.5f;
        else if ((val_edit == 2) && (DUT_cali[1].Litres < 80.0f))
            DUT_cali[1].Litres += 0.5f;
        break;
    case BUTTON_DN:
        if ((val_edit == 0) && (menu_sel < 5))
            menu_sel++;
        else if ((val_edit == 1) && (DUT_cali[0].Litres > 0.f))
            DUT_cali[0].Litres -= 0.5f;
        else if ((val_edit == 2) && (DUT_cali[1].Litres > 0.f))
            DUT_cali[1].Litres -= 0.5f;
        break;
    case BUTTON_OK:
        ok_pressed = 1;
        break;
    default:
        break;
    }
    button_pressed = BUTTON_NONE;

    if (ok_pressed == 1)
    {
        switch (menu_sel)
        {
        case 0:
            screen_draw(SCREEN_MENU);
            break;
        case 1:
            if (val_edit == 0)
                val_edit = 1;
            else
                val_edit = 0;
            break;
        case 2:
            DUT_cali[0].ADC_val = DUT_get_val();
            break;
        case 3:
            if (val_edit == 0)
                val_edit = 2;
            else
                val_edit = 0;
            break;
        case 4:
            DUT_cali[1].ADC_val = DUT_get_val();
            break;
        case 5:
            config.DUT_cali_ADC_1 = DUT_cali[0].ADC_val;
            config.DUT_cali_ADC_2 = DUT_cali[1].ADC_val;
            config.DUT_cali_Litres_1 = DUT_cali[0].Litres;
            config.DUT_cali_Litres_2 = DUT_cali[1].Litres;
            DUT_calibrate();
            screen_draw(SCREEN_MENU);
            break;
        default:
            break;
        }
    }
}

void draw_footer(uint8_t screen) // ������ ������ ������ � ��������� ������
{
    LCD_SetBGColor(GRAY);
    LCD_SetTextColor(WHITE);
    LCD_SetFont(&t_8x14_rus);

    LCD_SetTextPos(2, 226);
    uint8_t page_n = 0;
    switch (screen)
    {
    case SCREEN_MAIN:
        LCD_printf("��������");
        page_n = 0;
        break;
    case SCREEN_FUEL:
        LCD_printf("������");
        page_n = 1;
        break;
    case SCREEN_CNTA:
        LCD_printf("������� �");
        page_n = 2;
        break;
    case SCREEN_CNTB:
        LCD_printf("������� �");
        page_n = 3;
        break;
    case SCREEN_ERRORS:
        LCD_printf("������ ���");
        page_n = 4;
        break;
    }

    LCD_SetFont(&page_markers);
    LCD_SetTextPos(168, 227);

    for (uint8_t i = 0; i < 5; i++)
    {
        if (i == page_n)
            LCD_putchar(1);
        else
            LCD_putchar(0);
    }
}

void update_clock(void) // ��������� ����
{
    time_struct time;
    RTC_Get_Time(&time);
    LCD_SetBGColor(GRAY);
    LCD_SetTextColor(WHITE);
    LCD_SetTextPos(4, 4);

    LCD_SetFont(&clock_digits);
    LCD_printf("%02d", time.hour);

    LCD_SetFont(&clock_dots);
    if (time.sec % 2)
        LCD_putchar(1);
    else
        LCD_putchar(0);

    LCD_SetFont(&clock_digits);
    LCD_printf("%02d", time.min);

    LCD_SetTextPos(182, 18);
    LCD_SetFont(&t_16x22_digits);

    if (val_data.speed < 300.f) // ��������
        LCD_printf("%03.0f", val_data.speed);
    else
        LCD_printf("---");

}

void Config_write(void)
{
    memcpy(&config.cnt, &val_data.cnt, sizeof(val_data.cnt));

    config_ptr += sizeof(config);
    config.CRC8 = CRC8((uint8_t*) &config, sizeof(config) - 1);
    Flash_Unlock();
    Flash_Write((uint32_t) config_ptr, &config, sizeof(config));
    Flash_Lock();
    if (config_ptr + 2 * sizeof(config) > (uint8_t*) (CONFIG_BASE + CONFIG_SECTOR_LEN)) // ��������� �� ����� �� �� ����� �������
    {
        config_ptr = (uint8_t *) CONFIG_BASE;
        Flash_Unlock();
        Flash_Erase(CONFIG_BASE);
        Flash_Write(CONFIG_BASE, &config, sizeof(config));
        Flash_Lock();
    }
}

void Config_read(void)
{
    config_ptr = (uint8_t *) CONFIG_BASE;
    if ((config_ptr[0] != 0xAA) || (config_ptr[1] != CONFIG_VER))
    {
        Config_default();
        return;
    }
    while (config_ptr < (uint8_t*) (CONFIG_BASE + CONFIG_SECTOR_LEN - sizeof(config)))
    {
        if (*(config_ptr + sizeof(config)) != 0xAA)
            break;
        config_ptr += sizeof(config);
    }
    if ((config_ptr[0] != 0xAA) || (config_ptr[1] != CONFIG_VER))
        Config_default();
    else
    {
        Flash_Read((uint32_t) config_ptr, &config, sizeof(config));
        uint8_t crc = CRC8((uint8_t*) &config, sizeof(config) - 1);
        if (config.CRC8 != crc)
            Config_default(); // TODO: ������ ���������� ������
    }

    memcpy(&val_data.cnt, &config.cnt, sizeof(val_data.cnt));
}

/* ������������� �������� �� ��������� */
void Config_default(void)
{
    memset(&config, 0x00, sizeof(config));
    config.hdr = 0xAA;
    config.ver = CONFIG_VER;

    config.speed_pulses = 8.f;

    //TODO: ������������� ������ ��������

    config.CRC8 = CRC8((uint8_t*) &config, sizeof(config) - 1);

    Flash_Unlock();
    Flash_Erase(CONFIG_BASE);
    Flash_Write(CONFIG_BASE, &config, sizeof(config));
    Flash_Lock();
}

void Pwr_Off(void)
{
    __disable_irq();
    LCD_BL_OFF; // ��������� ���������
    KEY_BL_OFF;
    LCD_Sleep();

    LL_DBGMCU_EnableDBGStopMode(); // STOP MODE
    LL_LPM_EnableDeepSleep();
    LL_PWR_SetPowerMode(LL_PWR_MODE_STOP_LPREGU);

    LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_HSI); // ��������� HSE
    while (LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_HSI)
        ;
    LL_RCC_HSE_Disable();

    LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_0);
    while (LL_GPIO_IsInputPinSet(GPIOA, LL_GPIO_PIN_0)) // ���� �� ����� 1 - ����
        __WFE();

    NVIC_SystemReset();
}

void Clock_Init(void)
{
    LL_RCC_HSI_Enable(); // �� ������ ������
    while (LL_RCC_HSI_IsReady() != 1)
        ;
    LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_HSI); // �������� �� HSI
    while (LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_HSI)
        ;
    LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);

    LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_AFIO);
    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);

    LL_FLASH_EnablePrefetch(); // ����������� �����
    LL_FLASH_SetLatency(LL_FLASH_LATENCY_2);
    while (LL_FLASH_GetLatency() != LL_FLASH_LATENCY_2)
        ;

    LL_RCC_HSE_Enable(); // ������ ����� ���������� �� HSE+PLL
    while (LL_RCC_HSE_IsReady() != 1)
        ;

    LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSE_DIV_1, LL_RCC_PLL_MUL_9);
    LL_RCC_PLL_Enable();
    while (LL_RCC_PLL_IsReady() != 1)
        ;

    LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
    LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_2);
    LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);

    LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);
    while (LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL)
        ;

    LL_SetSystemCoreClock(72000000);
    LL_SYSTICK_SetClkSource(LL_SYSTICK_CLKSOURCE_HCLK);
    LL_Init1msTick(72000000);

    NVIC_SetPriority(SysTick_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0, 0));

    LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);
    LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOB);
    LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOC);
    LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOD);

    LL_GPIO_AF_Remap_SWJ_NOJTAG();
}

void MISC_Init(void)
{
    LL_GPIO_InitTypeDef GPIO_InitStruct;
    LL_EXTI_InitTypeDef EXTI_InitStruct;

    GPIO_InitStruct.Pin = LL_GPIO_PIN_2; // ��������� ������
    GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    LL_GPIO_Init(GPIOB, &GPIO_InitStruct);
    KEY_BL_OFF;

    GPIO_InitStruct.Pin = LL_GPIO_PIN_0; // IGN
    GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
    LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    EXTI_InitStruct.Line_0_31 = LL_EXTI_LINE_0; // ������� ��� ������ �� ���
    EXTI_InitStruct.Mode = LL_EXTI_MODE_EVENT;
    EXTI_InitStruct.Trigger = LL_EXTI_TRIGGER_FALLING;
    EXTI_InitStruct.LineCommand = ENABLE;
    LL_EXTI_Init(&EXTI_InitStruct);

    LL_GPIO_AF_SetEXTISource(LL_GPIO_AF_EXTI_PORTA, LL_GPIO_AF_EXTI_LINE0);
}

void button_Init(void)
{
    LL_GPIO_InitTypeDef GPIO_InitStruct;
    LL_TIM_InitTypeDef TIM_InitStruct;

    GPIO_InitStruct.Pin = LL_GPIO_PIN_10 | LL_GPIO_PIN_11 | LL_GPIO_PIN_12;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
    LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM2);

    TIM_InitStruct.Prescaler = 7200 - 1; // 72000000/7200/100 = 100 - 10��
    TIM_InitStruct.Autoreload = 100 - 1;
    TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
    TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
    TIM_InitStruct.RepetitionCounter = 0;
    LL_TIM_Init(TIM2, &TIM_InitStruct);

    LL_TIM_EnableIT_UPDATE(TIM2);
    NVIC_SetPriority(TIM2_IRQn, 0);
    NVIC_EnableIRQ(TIM2_IRQn);

    LL_TIM_EnableCounter(TIM2);
}

uint16_t button_press_cnt[3];
uint8_t button_long_press = 0;
void TIM2_IRQHandler(void)
{
    if (LL_TIM_IsActiveFlag_UPDATE(TIM2))
    {
        LL_TIM_ClearFlag_UPDATE(TIM2);
        uint8_t button_current = 0;
        for (uint8_t i = 0; i < 3; i++)
        {
            switch (i + 1)
            {
            case BUTTON_UP:
                button_current = LL_GPIO_IsInputPinSet(GPIOB, LL_GPIO_PIN_10);
                break;
            case BUTTON_DN:
                button_current = LL_GPIO_IsInputPinSet(GPIOB, LL_GPIO_PIN_12);
                break;
            case BUTTON_OK:
                button_current = LL_GPIO_IsInputPinSet(GPIOB, LL_GPIO_PIN_11);
                break;
            default:
                break;
            }
            if (button_current == 0) // ������ ������
            {
                if ((i == BUTTON_OK - 1) && (button_press_cnt[i] > 250))
                {
                    button_pressed = BUTTON_OK_LONG;
                    button_long_press = 1;
                }
                if (!button_long_press)
                {
                    button_press_cnt[i]++;
                    if ((i == BUTTON_UP - 1) || (i == BUTTON_DN - 1))
                    {
                        if (((button_press_cnt[i] > 100) && (button_press_cnt[i] % 10 == 0))
                                || ((button_press_cnt[i] > 300) && (button_press_cnt[i] % 3 == 0)))
                        {
                            button_pressed = i + 1; // ��������� ������������ ��� ������ �������
                        }
                    }
                }
            }
            else
            {
                if ((button_press_cnt[i] > 5) && (!button_long_press)) // ������ ��������
                {
                    button_pressed = i + 1;
                }
                button_press_cnt[i] = 0;
                if (i == BUTTON_OK - 1)
                    button_long_press = 0;
            }
        }
    }
}

void UART_Init(void)
{
    LL_USART_InitTypeDef USART_InitStruct;
    LL_GPIO_InitTypeDef GPIO_InitStruct;

    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_USART3);

    GPIO_InitStruct.Pin = LL_GPIO_PIN_10;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = LL_GPIO_PIN_11;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_FLOATING;
    LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    USART_InitStruct.BaudRate = 115200;
    USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
    USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
    USART_InitStruct.Parity = LL_USART_PARITY_NONE;
    USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
    USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
    LL_USART_Init(USART3, &USART_InitStruct);

    LL_USART_ConfigAsyncMode(USART3);
    LL_USART_Enable(USART3);
}

void _putchar(char character)
{
#ifdef _DEBUG_PRINTF_ENABLED
    while (!LL_USART_IsActiveFlag_TXE(USART3));
    LL_USART_TransmitData8(USART3,character);
#endif
}

void HardFault_Handler(void)
{
    LCD_SetTextColor(WHITE);
    LCD_SetFont(&t_12x24_full);

    LCD_SetTextPos(20, 20);
    LCD_SetBGColor(RED);
    LCD_printf("PIZDETS");

    while (1)
        ;
    NVIC_SystemReset();
}
