#ifndef SPEED_FUEL_H_
#define SPEED_FUEL_H_

#include "stm_lib.h"

typedef struct __attribute__((packed))
{
	uint32_t dist;
	uint32_t fuel;
	uint32_t time;
}cnt_data_t;

typedef struct
{
	float speed;
	float speed_cali;
	float speed_cali_coef;
	float fuel_moment;
	float fuel_cur;
	float fuel_avg;
	float Vbat;
	float fuel_level;
	float dist_total;
	cnt_data_t cnt[2];

} vals_data_t;

uint32_t dist_cnt_get(void);
void dist_cnt_reset(void);

void Speed_Fuel_proc(void);

void Speed_Fuel_Init(void);

uint16_t DUT_get_val(void);
void DUT_calibrate(void);
void ADC_Init(void);

#endif /* SPEED_FUEL_H_ */
