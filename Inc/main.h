#ifndef __MAIN_H__
#define __MAIN_H__

#include "lcd.h"
#include "stm_lib.h"
#include "speed_fuel.h"

//#define _DEBUG_PRINTF_ENABLED
//#define DEBUG_BOARD

#define LOWPASS(xold, xnew, pow2) \
   ((((1<<pow2) - 1) * xold +  xnew) >> pow2) // ������� ���-������

#define LOWPASS_F(xold, xnew, k) ((1.f-k)*xold + k*xnew) // ������� ������ - float

#define PULSES_PER_LITER (12000.f)

#define FUEL_BUFFER_LEN (60)

#define CONFIG_BASE 		0x0800FC00
#define CONFIG_SECTOR_LEN	1024

#define CONFIG_VER 0x01

#define KEY_BL_OFF 	LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_2)

#ifndef DEBUG_BOARD
	#define KEY_BL_ON	 	LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_2)
#else
	#define KEY_BL_ON		KEY_BL_OFF
#endif

typedef struct __attribute__((packed)) // LEN = 56
{
    uint8_t hdr; // ��������� 0xAA
    uint8_t ver; // ������ ��������
    uint8_t reserved_0[2]; // ��� ������������

    cnt_data_t cnt[2]; // �������� ������/�������/�����

    float speed_pulses; // ���������� ������� ��������

    float DUT_cali_Litres_1; // ���������� ���
    float DUT_cali_Litres_2;
    uint16_t DUT_cali_ADC_1;
    uint16_t DUT_cali_ADC_2;

    float DUT_cali_K;
    float DUT_cali_B;

    uint8_t reserved_2[3]; // ��� ������������

    uint8_t CRC8;

}Config_type;

typedef struct
{
	uint16_t ADC_val;
	float Litres;
}DUT_cali_point_type;

typedef enum
{
	SCREEN_LOGO,
	SCREEN_MAIN,
	SCREEN_FUEL,
	SCREEN_CNTA,
	SCREEN_CNTB,
	SCREEN_ERRORS,
	SCREEN_MENU,
	SCREEN_SETTIME,
	SCREEN_CFGSPEED,
	SCREEN_CFGDUT,
}screen;

typedef enum
{
	BUTTON_NONE = 0,
	BUTTON_UP,
	BUTTON_DN,
	BUTTON_OK,
	BUTTON_OK_LONG
}button;

extern Config_type config;

#endif /* __MAIN_H__ */
