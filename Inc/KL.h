#ifndef KL_H_
#define KL_H_

#include "stm_lib.h"

#define KL_BUFFERS 5
#define KL_BUFFER_LEN 64

typedef enum
{
	KL_ERROR,
	KL_DISCONNECTED,
	KL_INIT_SENT,
	KL_INIT_WAIT,
	KL_IDLE,
	KL_REQUEST_SENT,
	KL_REQUEST_WAIT,
}kl_state;

typedef struct kl_data_t
{
	uint16_t err_cnt;

	uint16_t RPM;
	uint8_t speed;
	uint8_t tr_pos;
	float Vbat;
	float T_w;
	float T_a;

}kl_data_t;

void KL_HW_Init(void);
uint8_t KL_main_proc(void);

#endif /* KL_H_ */
