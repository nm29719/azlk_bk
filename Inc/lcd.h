#ifndef __LCD_H
#define __LCD_H

#include "main.h"
#include "stm_lib.h"
#include "lcd_image.h"
#include "lcd_font.h"

//#ifndef DEBUG_BOARD ���� ���
	#define LCD_W (240)
	#define LCD_H (240)

#define LCD_TURN_0
//#define LCD_TURN_90
//#define LCD_TURN_180
//#define LCD_TURN_270

#define SAVE_X_OFFSET

#define LCD_BL_OFF 		LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_0)
#define LCD_BL_ON	 	LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_0)
#define LCD_DC_DN 		LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_6)
#define LCD_DC_UP 		LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_6)
#define SPI1_BSY 		(SPI1->SR & SPI_SR_BSY)
#define SPI1_TXE 		(SPI1->SR & SPI_SR_TXE)

#define RED   0xF800
#define GREEN 0x07E0
#define BLUE  0x001E
#define BLACK 0x0000
#define WHITE 0xFFFF
#define GRAY  0x630C
#define ORANGE 0xFBE0

#define LCD_printf(...) fctprintf(&LCD_out, NULL, __VA_ARGS__)

void LCD_init(void);
void LCD_HWinit(void);
void LCD_SendCMD(uint8_t val);
void LCD_SendData(uint8_t val);
void LCD_SendPixel(uint16_t data);
void LCD_Sleep(void);
void LCD_SetWindow(uint16_t x, uint16_t y, uint16_t w, uint16_t h);
void LCD_Fill(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t color);
void LCD_putchar(char chr);
void LCD_SendChar(uint8_t * start);
void LCD_Image_mono(tImage img, uint16_t x, uint16_t y);
void LCD_print(char * str);
void LCD_out(char character, void* arg);
//void LCD_printf(const char *pFormat, ...);
void LCD_SetTextColor(uint16_t color);
void LCD_SetBGColor(uint16_t color);
void LCD_SetTextPos(uint16_t x, uint16_t y);
void LCD_SetFont(const tFont * fnt);

#endif
